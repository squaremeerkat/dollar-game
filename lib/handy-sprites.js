"use strict";

var handySprites = {};
var hs = handySprites;

handySprites.init = function() {
	this.baseSize = 720;
	this.fontColors = {
		"white": "#d29a71",
		"black": "#352e2e",
		"coins": "#ffcc00",
		"minus": "#ff7c35",
		"blue": "#a2c3ff",
		"collect": "#80c5ff",
		"spread": "#69d482"
	}
	this.fontType = {
		"default": 'sans-serif',
		// "default": "'Kadwa', serif",
		// "default": "'Dekko', sans-serif",
		// "default": "'Cutive', serif",
		// "default": "'Coustard', serif",
		// "big": '"Passion One", sans-serif',
		"big": "'Coustard', serif",
		"small": '"Open Sans", sans-serif'
	}
}
handySprites.init();

handySprites.resizeCallback = function(isForced) {
	var newGameSize = this.getNewGameSize();
	if ((newGameSize.width === game.width) && (newGameSize.height === game.height)) {
		return;
	} else {
		game.scale.setGameSize(newGameSize.width, newGameSize.height);
		if (game.state.current === 'preloaderState' &&preloaderState && preloaderState.resize)
			preloaderState.resize();
		if (game.state.current === 'gameState' && gameState && gameState.resize)
			gameState.resize();
	}
}

handySprites.getNewGameSize = function() {
	var ratio, width, height;
	ratio = window.innerWidth / window.innerHeight;
	if (ratio > 1) {
		width = Math.round(this.baseSize * ratio);
		height = this.baseSize;
	} else {
		height = Math.round(this.baseSize / ratio);
		width = this.baseSize;
	}
	return {width: width, height: height}
}

handySprites.getSpritesheetName = function(frame) {
	for (var spritesheet in this.spritesheetList) {
		if (this.spritesheetList[spritesheet].frames[frame]) {
			return spritesheet;
		}
	}
}

handySprites.loadFromLS = function(type, str) {
	var catalogue = "img";
	if (type === "dev image")
		catalogue = "img_dev/";
	var imagesArr = str
		.replace(/(\.(png|jpg|jpeg|gif|webp))( +)?/g, "$1|")
		.split("|")
		.slice(0,-1);
	var imageName, imageNameFull;
	for (var i = 0; i < imagesArr.length; i++) {
		imageName = imagesArr[i].split(".")[0];
		imageNameFull = imagesArr[i];
		if (type === "dev image") {
			game.load.image(imageName, catalogue + imagesArr[i] + jsonVersionToReload);
		}
	}
}

handySprites.goFullScreen = function() {
	if ((!game.scale.isFullScreen)&&(!game.device.desktop)) {
		game.scale.startFullScreen();
	}
}

handySprites.checkFPS = function() {
	game.time.physicsElapsed = (game.time.elapsedMS < 100 ? game.time.elapsedMS : 100)/1000;
}

handySprites.makeGroup = function(a) {
	if (a === undefined)
		a = {};
	a.x = a.x || 0;
	a.y = a.y || 0;
	var group;
	group = game.add.group();
	group.x = a.x;
	group.y = a.y;
	if (a.scale !== undefined)
		group.scale.set(a.scale);
	if (a.group) a.group.add(group);
	if (a.sprite) {
		if (a.sprite.constructor !== Array)
			a.sprite = [a.sprite];
		var sprites = [];
		for (var i in a.sprite) {
			var s = a.sprite[i];
			s.group = group;
			if (s.text)
				sprites[i] = this.makeText(s);
			if (s.frame)
				sprites[i] = this.makeSprite(s);
		}
	}
	return group;
}

handySprites.makeSprite = function(a) {
	a.x = a.x || 0;
	a.y = a.y || 0;
	var sprite;
	sprite = game.add.sprite(a.x, a.y, a.frame);
	if (a.anchorX === undefined)
		a.anchorX = .5;
	if (a.anchorY === undefined)
		a.anchorY = .5;
	sprite.anchor.set(a.anchorX, a.anchorY);
	if (a.scale !== undefined)
		sprite.scale.set(a.scale);
	if (a.group) a.group.add(sprite);
	if (a.alpha !== undefined)
		sprite.alpha = a.alpha;
	return sprite;
}

handySprites.makeText = function(a) {
	a.x = a.x || 0;
	a.y = a.y || 0;
	if (a.anchorX === undefined)
		a.anchorX = .5;
	if (a.anchorY === undefined)
		a.anchorY = .5;
	a.fontSize = a.fontSize || 20;
	a.color = a.color || "white";
	a.font = a.font || "default";
	a.align = a.align || "center";
	var text;
	var font = {font:"" + a.fontSize + "px " + this.fontType[a.font], fill:this.fontColors[a.color], align:a.align};
	if (a.isBold)
		font.fontWeight = "bold";
	if (a.style)
		for (var styleName in a.style)
			font[styleName] = a.style[styleName];
	text = game.add.text(a.x, a.y, this.getText(a.text), font);
	text.anchor.set(a.anchorX, a.anchorY);
	if (a.maxWidth) {
		text.maxWidth = a.maxWidth;
		this.fitText(text);
	}
	if (a.maxHeight) {
		text.maxHeight = a.maxHeight;
		this.fitText(text);
	}
	if (a.group) a.group.add(text);
	return text;
}

handySprites.getText = function(text) {
	return text;
}

handySprites.fitText = function(textObject) {
	textObject.scale.set(1);
	if (textObject.maxWidth)
		if (textObject.width > textObject.maxWidth)
			textObject.scale.set(textObject.maxWidth / textObject.width);
	if (textObject.maxHeight)
		if (textObject.height > textObject.maxHeight)
			textObject.scale.set(textObject.scale.x*(textObject.maxHeight / textObject.height));
}

handySprites.changeText = function(a) {
	a.object.text = this.getText(a.text);
	this.fitText(a.object);
}

handySprites.makeButton = function(a) {
	a.x = a.x || 0;
	a.y = a.y || 0;
	var group = this.makeGroup({x:a.x, y:a.y, group:a.group});
	if (a.sprite.constructor !== Array)
		a.sprite = [a.sprite];
	var sprites = [];
	for (var num in a.sprite) {
		var s = a.sprite[num];
		s.group = group;
		if (s.text)
			sprites[num] = this.makeText(s);
		if (s.frame)
			sprites[num] = this.makeSprite(s);
	}
	var buttonSprite = sprites[0];
	buttonSprite.inputEnabled = true;
	buttonSprite.input.useHandCursor = true;
	buttonSprite.events.onInputDown.add(function() {
		if (a.context)
			a.func.apply(a.context);
		else
			a.func();
		// audio.play("click");
	});
	buttonSprite.events.onInputOver.add(function(){
		game.add.tween(group.scale).to({x:1.05, y:1.05}, 200, 
			Phaser.Easing.Quadratic.InOut, true);
	});
	buttonSprite.events.onInputOut.add(function(){
		game.add.tween(group.scale).to({x:1, y:1}, 300, 
			Phaser.Easing.Quadratic.InOut, true);
	});
	if (a.size) {
		if (a.size.constructor !== Object)
			a.size = {x:a.size, y:a.size};
		buttonSprite.hitArea = new Phaser.Rectangle(-a.size.x/2, -a.size.y/2, a.size.x, a.size.x);
	}
	return group;
}

handySprites.rInt = function(a, b) {
	return Math.floor(Math.random() * (b + 1 - a)) + a;  
}
