"use strict";

var graph = {};

graph.show = function() {
	if (this.replayTimer)
		game.time.events.remove(this.replayTimer);
	graph.calculateVisibleSize();
	this.connectionsShowed = {};
	this.groupOuter = this.groupOuter || hs.makeGroup({group:centerGroup});
	if (this.group)
		this.group.destroy(true);
	this.group = hs.makeGroup({group:this.groupOuter});
	var text, color;
	for (var i = 0; i < this.graph.length; i++) {
		this.showConnections(i);
	}
	for (var i = 0; i < this.graph.length; i++) {
		text = this.graph[i].balance;
		if (text < 0)
			color = "minus";
		else
			color = "white";
		text = "" + text;
		this.graph[i].button = hs.makeButton({sprite:[{frame:"circle"}], group:this.group, x:this.graph[i].x, y:this.graph[i].y, func:
		(function() {
			var _i = i;
			return function() {
				this.press(_i);
			}
		})(i), context:this});
		this.graph[i].text = hs.makeText({text:text, fontSize:60, maxWidth:70, y:3, color:color, group:this.graph[i].button});
	}
	if (ui.sumText)
		ui.sumText.destroy();
	var strBelow = "";
	strBelow += "sum: " + this.getBalanceSum();
	this.saveAllConnections();
	var connNum = 0;
	for (var c in this.connectionsSaved)
		connNum++;
	strBelow += ", edges: " + connNum;
	strBelow += ", vertices: " + this.graph.length;
	strBelow += ", genus: " + (connNum - this.graph.length + 1);

	ui.sumText = hs.makeText({text:strBelow, x:-350, anchorX:0, fontSize:20, color:"black", group:centerGroup});
	ui.resize();
	this.resize();
}

graph.resize = function() {
	if (!this.group)
		return;
	var scale = Math.min(game.width/(this.borders.width+80*2), (game.height-100)/(this.borders.height+80*2));
	this.group.scale.set(scale);
	this.group.x = -this.borders.minX*scale;
	this.group.y = -this.borders.minY*scale;
	this.groupOuter.x = -this.borders.width*scale/2;
	this.groupOuter.y = -this.borders.height*scale/2+0;
}

graph.addOneConnection = function(first, second) {
	if (!this.graph[first].links)
		this.graph[first].links = {};
	if (!this.graph[first].links[""+second])
		this.graph[first].links[""+second] = 1;
}

graph.addConnection = function(first, second) {
	if (first === second)
		return;
	graph.addOneConnection(first, second);
	graph.addOneConnection(second, first);
}

graph.showConnection = function(first, second) {
	if (first === second)
		return;
	var connectionName;
	if (first > second)
		connectionName = ""+second+"_"+first;
	else
		connectionName = ""+first+"_"+second;
	if (this.connectionsShowed[connectionName])
		return;
	this.connectionsShowed[connectionName] = 1;
	var connection = hs.makeSprite({frame:"line", group:this.group, x:this.graph[first].x, y:this.graph[first].y, anchorX:0});
	var size = connection.width;
	var distance = game.math.distance(this.graph[first].x, this.graph[first].y, this.graph[second].x, this.graph[second].y)
	var angle = game.math.angleBetween(this.graph[first].x, this.graph[first].y, this.graph[second].x, this.graph[second].y)
	connection.scale.set(distance/size, 1);
	connection.angle = game.math.radToDeg(angle);
}

graph.showConnections = function(position) {
	var links = this.graph[position].links;
	for (var second in links) {
		this.showConnection(position, +second);
	}
}

graph.expand = function(minDist) {
	for (var i = 0; i < this.maxPoints; i++) {
		var p1 = this.graph[i];
		for (var j = 0; j < this.maxPoints; j++) {
			if (i === j)
				continue;
			var p2 = this.graph[j];
			var dist = game.math.distance(p1.x, p1.y, p2.x, p2.y);
			if (dist > minDist)
				continue;
			this.graph[i].x = p1.x + (p1.x-p2.x)*(1/dist);
			this.graph[i].y = p1.y + (p1.y-p2.y)*(1/dist);
		}
	}
}

graph.updateText = function(position) {
	if (this.graph[position].text)
		this.graph[position].text.destroy();
	var text, color;
	text = this.graph[position].balance;
	if (text < 0)
		color = "minus";
	else
		color = "white";
	text = "" + text;
	this.graph[position].text = hs.makeText({text:text, fontSize:60, maxWidth:70, y:5, color:color, group:this.graph[position].button});
}

graph.checkWin = function() {
	for (var i = 0; i < this.graph.length; i++)
		if (this.graph[i].balance < 0)
			return;
	this.isWin = true;
	if (this.isFromVideo && !this.isReplay) {
		if (!this.bestSolution || this.solution.length < this.bestSolution.length)
			this.bestSolution = this.solution.slice();
			storage.set("bestSolution", this.bestSolution);
	}
	ui.showWin();
}

graph.stepPlus = function() {
	this.stepNum++;
	ui.updateSteps();
}

graph.restart = function() {
	this.group.destroy(true);
	this.isWin = false;
	this.solution = [];
	this.stepNum = 0;
	if (this.isFromVideo) {
		this.load(graphFromVideo);
		storage.set("savedGraphFromVideo", this.save());
	} else {
		var graphSaved;
		graphSaved = storage.get("savedGraphRandomStart");
		this.solution = [];
		if (graphSaved)
			this.load(graphSaved);
		else
			this.createRandom();
		graphSaved = this.save();
		storage.set("savedGraphRandomStart", graphSaved);
		storage.set("savedGraphRandom", graphSaved);
	}
	this.show();
	ui.updateSteps();
	ui.clearWin();
}

graph.copyToClipboard = function(str) {
	var el = document.createElement('textarea');
	el.value = str;
	document.body.appendChild(el);
	el.select();
	document.execCommand('copy');
	document.body.removeChild(el);
}

graph.save = function() {
	var points = [];
	var connections = []; //0_1
	for (var i = 0; i < this.graph.length; i++)
		points[i] = [Math.round(this.graph[i].x), Math.round(this.graph[i].y), this.graph[i].balance]
	this.saveAllConnections();
	for (var c in this.connectionsSaved)
		connections.push(c);
	var result = JSON.stringify({points:points, connections:connections, solution:this.solution, isWin:this.isWin});
	return result;
}

graph.load = function(input, isFromVideo) {
	var loadedGraph = JSON.parse(input);
	this.graph = [];
	for (var i = 0; i < loadedGraph.points.length; i++) {
		var point = {};
		this.graph[i] = point;
		point.x = loadedGraph.points[i][0];
		point.y = loadedGraph.points[i][1];
		point.balance = loadedGraph.points[i][2];
	}
	for (var i = 0; i < loadedGraph.connections.length; i++) {
		var c = loadedGraph.connections[i].split("_");
		this.addConnection(+c[0], +c[1]);
	}
	if (loadedGraph.isWin)
		this.isWin = loadedGraph.isWin;
	this.stepNum = 0;
	this.solution = [];
	if (loadedGraph.solution) {
		this.solution = loadedGraph.solution.slice();
		if (loadedGraph.solution.length > 0) {
			this.stepNum = loadedGraph.solution.length;
		}
	}
}

graph.calculateVisibleSize = function() {
	this.borders = {minX:10000,maxX:-10000,minY:10000,maxY:-10000};
	for (var i = 0; i < this.graph.length; i++) {
		if (this.graph[i].x < this.borders.minX)
			this.borders.minX = this.graph[i].x;
		if (this.graph[i].x > this.borders.maxX)
			this.borders.maxX = this.graph[i].x;
		if (this.graph[i].y < this.borders.minY)
			this.borders.minY = this.graph[i].y;
		if (this.graph[i].y > this.borders.maxY)
			this.borders.maxY = this.graph[i].y;
	}
	this.borders.width = this.borders.maxX - this.borders.minX;
	this.borders.height = this.borders.maxY - this.borders.minY;
}

graph.getBalanceSum = function() {
	var balance = 0;
	for (var i = 0; i < this.graph.length; i++)
		balance += this.graph[i].balance;
	return balance;
}

graph.spreadSilent = function(position) {
	if (position === undefined)
		return;
	if (this.graph[position].balance <= 0)
		return;
	this.stepPlus();
	var links = this.graph[position].links;
	for (var second in links) {
		this.graph[position].balance--;
		this.graph[+second].balance++;
	}
}

graph.press = function(position) {
	if (this.isWin || this.isReplay)
		return;
	this.spread(position);
	this.checkWin();
	this.saveLocal();
}

graph.saveLocal = function() {
	var graphSaved = graph.save();
	if (this.isFromVideo)
		storage.set("savedGraphFromVideo", graphSaved);
	else
		storage.set("savedGraphRandom", graphSaved);
}

graph.loadLocal = function() {
	var graphSaved;
	if (this.isFromVideo) {
		graphSaved = storage.get("savedGraphFromVideo");
		if (graphSaved)
			graph.load(graphSaved)
		else
			graph.load(graphFromVideo)
	} else {
		graphSaved = storage.get("savedGraphRandom");
		if (graphSaved)
			graph.load(graphSaved)
		else
			graph.createRandom();
	}
}

graph.newRandom = function() {
	this.restart();
}

graph.saveAllConnections = function() {
	this.connectionsSaved = {};
	for (var i = 0; i < this.graph.length; i++) {
		this.saveConnections(i);
	}
}

graph.saveConnections = function(position) {
	var links = this.graph[position].links;
	for (var second in links) {
		this.saveConnection(position, +second);
	}
}

graph.saveConnection = function(first, second) {
	if (first === second)
		return;
	var connectionName;
	if (first > second)
		connectionName = ""+second+"_"+first;
	else
		connectionName = ""+first+"_"+second;
	if (this.connectionsSaved[connectionName])
		return;
	this.connectionsSaved[connectionName] = 1;
}

graph.getNearestPoint = function(position) {
	var resultPosition;
	for (var minConnections = 0; minConnections < 5; minConnections++) {
		var minDistance = 10000;
		for (var i = 0; i < this.graph.length; i++) {
			if (i === position)
				continue;
			var connNum = 0;
			if (this.graph[i].links)
				for (var c in this.graph[i].links)
					connNum++;
			if (connNum !== minConnections)
				continue;
			var dist = game.math.distance(this.graph[position].x, this.graph[position].y, this.graph[i].x, this.graph[i].y);
			if (dist < minDistance) {
				resultPosition = i;
				minDistance = dist;
			}
		}
		if (resultPosition !== undefined)
			return resultPosition;
	}
}

graph.createRandom = function() {
	// create graph
	this.graph = [];
	this.maxPoints = hs.rInt(5,20);
	var graphSize = Math.round(100+this.maxPoints/5*50);
	for (var i = 0; i < this.maxPoints; i++) {
		var point = {};
		this.graph[i] = point;
		point.x = hs.rInt(-graphSize, graphSize);
		point.y = hs.rInt(-graphSize, graphSize);
		point.balance = hs.rInt(-4, 5);
	}

	// expand points
	for (var i = 0; i < 50; i++)
		this.expand(80*2);
	for (var i = 0; i < 50; i++)
		this.expand(80*1.5);
	for (var i = 0; i < 50; i++)
		this.expand(80*1.2);

	var connectionsNeeded = Math.max(this.graph.length, Math.floor(this.maxPoints*hs.rInt(10,20)/10));

	// add connections to each point
	for (var i = 0; i < this.graph.length; i++) {
		var second = this.getNearestPoint(i);
		this.addConnection(i, second);
	}

	// add additional connections
	for (var i = 0; i < connectionsNeeded-this.graph.length; i++) {
		var first = hs.rInt(0,this.graph.length-1);
		var second = this.getNearestPoint(first);
		this.addConnection(first, second);
	}

	// make more connections than dots
	var attempts = 0;
	while (this.getConnectionsNum() < connectionsNeeded && attempts < 1000) {
		attempts++;
		var first = hs.rInt(0, this.graph.length-1);
		first = this.getNearestPoint(first);
		var second = this.getNearestPoint(first);
		this.addConnection(first, second);
	}

	// connect islands
	var islands = this.findAloneIslands();
	if (islands.length > 1) {
		for (var i = 0; i < islands.length-1; i++) {
			this.addConnection(
				this.getRandomElemInObject(islands[i]),
				this.getRandomElemInObject(islands[i+1]));
		}
	}

	// normalize balance sum
	this.saveAllConnections();
	var connNum = 0;
	for (var c in this.connectionsSaved)
		connNum++;
	var genus = connNum - this.graph.length + 1;
	var balanceNeeded = Math.max(1, genus+hs.rInt(-1,1));
	while (this.getBalanceSum() < balanceNeeded || this.getBalanceSum() > balanceNeeded) {
		var i = hs.rInt(0, this.graph.length-1);
		if (this.getBalanceSum() < balanceNeeded) {
			this.graph[i].balance++;
		}
		if (this.getBalanceSum() > balanceNeeded) {
			this.graph[i].balance--;
		}
	}
	this.solution = [];
	this.stepNum = 0;
	this.isWin = false;
	ui.updateSteps();
	storage.set("savedGraphRandomStart", this.save());
}

graph.getConnectionsNum = function() {
	this.saveAllConnections();
	var connNum = 0;
	for (var c in this.connectionsSaved)
		connNum++;
	return connNum
}

graph.findAloneIslands = function() {
	var attempts = 0;
	var allP = {};
	for (var i = 0; i < this.graph.length; i++)
		allP[""+i] = 1;
	var islands = [];
	var currentIslandNum = 0;
	for (var i = 0; i < this.graph.length; i++) {
		var tempArr = [];
		for (var p in allP) {
			if (allP[p] === 1)
				tempArr.push(p);
		}
		if (tempArr.length === 0) {
			return islands;
		}
		islands[currentIslandNum] = {};
		var randP = tempArr[hs.rInt(0, tempArr.length-1)];
		recursiveGetConnectedPoint(+randP);
		currentIslandNum++;
		if (currentIslandNum > 100) {
			console.log("too much islands!");
			return islands;
		}
	}
	function recursiveGetConnectedPoint(position) {
		attempts++;
		if (attempts > 100)
			return;
		if (allP[""+position] === 2)
			return;
		allP[""+position] = 2;
		islands[currentIslandNum][""+position] = 1;
		for (var c in graph.graph[position].links)
			recursiveGetConnectedPoint(c);
	}
}

graph.getRandomElemInObject = function(obj) {
	var tempArr = [];
	for (var p in obj) {
		tempArr.push(p);
	}
	if (tempArr.length === 0) {
		return;
	}
	return tempArr[hs.rInt(0, tempArr.length-1)];
}

graph.playReplay = function() {
	if (!this.bestSolution)
		return;
	this.isReplay = true;
	this.replayNum = 0;
	this.stepNum = 0;
	this.isWin = false;
	ui.updateSteps();
	this.replayTimer = game.time.events.loop(350, function() {
		if (this.bestSolution[this.replayNum] === undefined || this.replayNum >= this.bestSolution.length) {
			game.time.events.remove(this.replayTimer);
			return
		}
		this.spread(this.bestSolution[this.replayNum], 300);
		this.checkWin();
		this.replayNum++;
	}, this);
}

graph.spread = function(position, duration) {
	if (position === undefined)
		return;
	this.stepPlus();
	if (duration === undefined)
		duration = 500;
	var solutionPos = position;
	if (this.isCollectMode)
		solutionPos = -solutionPos-1; // save positions in collect mode as minus and -1; 0 -> -1, 1 -> -2...
	if (!this.isReplay)
		this.solution.push(solutionPos);
	var p1, p2, pos;
	if (position >= 0)
		pos = position;
	else
		pos = -position-1;
	var links = this.graph[pos].links;
	for (var second in links) {
		if (!this.isReplay) {
			if (!this.isCollectMode) {
				p1 = position;
				p2 = +second;
			} else {
				p1 = +second;
				p2 = position;
			}
		} else {
			if (position >= 0) {
				p1 = position;
				p2 = +second;
			} else {
				p1 = +second;
				p2 = -position-1; // restore position from minus for collect mode;
			}
		}
		this.graph[p1].balance--;
		this.graph[p2].balance++;
		this.updateText(p1);
		var dollar = hs.makeSprite({frame:"dollar", group:this.group, x:this.graph[p1].x, y:this.graph[p1].y});
		game.add.tween(dollar)
			.to({x:this.graph[p2].x, y:this.graph[p2].y}, duration, Phaser.Easing.Cubic.InOut, true)
			.onComplete.add((function(p2) {
				var second2 = p2;
				return function(sprite) {
					this.updateText(second2);
					sprite.destroy();
				}
			})(p2), this);
	}
}