"use strict";

var game;

startGame = function() {
	var newGameSize = hs.getNewGameSize();
	game = new Phaser.Game(
		newGameSize.width, 
		newGameSize.height, 
		Phaser.CANVAS
		);
	game.state.add("bootState", {
		preload: function() {bootState.preload()},
		create: function() {bootState.create()}
	});
	game.state.add("preloaderState", {
		preload: function() {preloaderState.preload()},
		create: function() {preloaderState.create()}
	});
	game.state.add("gameState", {
		create: function() {gameState.create()},
		update: function() {gameState.update()}
	});
	game.state.start("bootState");
	// waitForWebfonts([ "Passion One", "Open Sans" ], function() {
	// 	game.state.start('bootState');
	// });
}