"use strict";

var centerGroup;
var gameState = {};

gameState.create = function() {
	game.input.onDown.add(hs.goFullScreen, this);
	game.stage.backgroundColor = "#d29a71";
	this.background = game.add.tileSprite(0, 0, game.width, game.height, "craft_paper");
	centerGroup = hs.makeGroup();

	graph.bestSolution = storage.get("bestSolution");
	// if (!graph.bestSolution || !graph.bestSolution.length)
	// 	graph.bestSolution = [1,6,3,1,4,9,4,3,7,4,4,1,1,3,3,9,4,9,4,8,2,7,3,0,8,6,6,9,3];
	ui.createMenu();
	// ui.create();

	// graph.create();
	// graph.show();

	this.resize();
}

gameState.resize = function() {
	centerGroup.x = game.width/2;
	centerGroup.y = game.height/2;
	this.background.width = game.width;
	this.background.height = game.height;
	graph.resize();
	ui.resize();
}

gameState.update = function() {
	hs.checkFPS();
}