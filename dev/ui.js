"use strict";

var ui = {};

ui.create = function() {
	this.group = hs.makeGroup({group:centerGroup});
	this.stepsText = hs.makeText({text:"steps: 0", x:-350, anchorX:0, fontSize:30, color:"black", group:this.group});
	if (!graph.isReplay)
		this.restartButton = hs.makeButton({sprite:[{text:"restart", fontSize:30, color:"black"}], group:this.group, x:170, func:function() {
			if (graph.isReplay)
				return;
			if (graph.stepNum > 0)
				this.createRestartConfirm();
			else
				graph.restart();
		}, context:this});
	this.menuButton = hs.makeButton({sprite:[{text:"menu", fontSize:30, color:"black"}], group:this.group, x:300, func:function() {
		game.time.events.remove(graph.replayTimer);
		this.group.destroy(true);
		this.createMenu();
		graph.group.destroy(true);
		if (this.sumText)
			this.sumText.destroy();
		if (this.changeModeButton)
			this.changeModeButton.destroy();
	}, context:this});
	if (!graph.isFromVideo)
		this.newRandomButton = hs.makeButton({sprite:[{text:"new", fontSize:30, color:"black"}], group:this.group, x:50, func:function() {
			if (graph.stepNum > 0)
				this.createNewRandomConfirm();
			else
				ui.pressNewRandom();
		}, context:this});
	// change collect and spread mode
	this.changeModeButton = hs.makeButton({sprite:[{frame:"button"}], group:centerGroup, x:720/2-30-186/2, func:this.changeMode, context:this});
	this.collectModeText = hs.makeText({text:"> collect <", y:2, maxWidth:150, fontSize:30, color:"collect", group:this.changeModeButton});
	this.spreadModeText = hs.makeText({text:"< spread >", y:2, maxWidth:150, fontSize:30, color:"spread", group:this.changeModeButton});
	this.showMode();
	this.resize();
}

ui.resize = function() {
	if (this.group)
		this.group.y = -game.height/2+50;
	if (this.sumText)
		this.sumText.y = game.height/2-25;
	if (this.changeModeButton)
		this.changeModeButton.y = game.height/2-30-15;
}

ui.showWin = function() {
	console.log("win");
	this.winText = hs.makeText({text:"win!", fontSize:30, color:"black", group:this.group, x:this.stepsText.x+this.stepsText.width+30, anchorX:0, isBold:true});
	this.shotConfetti(30);
}

ui.updateSteps = function() {
	hs.changeText({object:this.stepsText, text:"steps: "+graph.stepNum});
}

ui.makeConfetti = function(a) {

	var confettiColors = [0xfb1b09, 0xf8c92c, 0x1096e5, 0x5dbcf3];

	var confettiGroup = hs.makeGroup({group:centerGroup});

	var confetti = game.add.graphics(0, 0);
	confetti.beginFill(confettiColors[hs.rInt(0, confettiColors.length-1)]);
	if (hs.rInt(0, 1) === 0)
		confetti.drawRect(-10, -20, 20, 40);
	else
		confetti.drawCircle(0, 0, 30);
	confetti.endFill();	
	confetti.angle = hs.rInt(0, 360);

	var confettiGroupInner = hs.makeGroup({group:confettiGroup});
	confettiGroupInner.add(confetti);
	game.add.tween(confettiGroupInner.scale)
		.to({x:-1, y:1}, hs.rInt(500, 1000), Phaser.Easing.Quadratic.InOut, true, 0, -1, true);
	confettiGroupInner.angle = hs.rInt(0, 360);
	game.add.tween(confettiGroupInner)
		.to({angle: (hs.rInt(0, 1) === 0) ? "+360" : "-360"}, hs.rInt(2000, 4000), Phaser.Easing.Linear.None, true, 0, -1);

	confettiGroup.x = (hs.rInt(0, 1) === 0) ? game.width/2+30 : -(game.width/2+30);
	confettiGroup.y = hs.rInt(-game.height/2, 0);
	game.add.tween(confettiGroup)
		.to({x:hs.rInt(-game.width/2, game.width/2)}, hs.rInt(2000, 4000), Phaser.Easing.Quadratic.Out, true);
	game.add.tween(confettiGroup)
		.to({y:game.height/2+40}, hs.rInt(2000, 4000), Phaser.Easing.Quadratic.In, true)
		.onComplete.add(function(group) {
			group.destroy(true);
		}, this);
}

ui.shotConfetti = function(num) {
	for (var i = 0; i < num; i++) {
		this.makeConfetti();
	}
}

ui.clearWin = function() {
	if (this.winText)
		this.winText.destroy();
}

ui.createMenu = function() {
	if (this.replayTimer)
		game.time.events.remove(this.replayTimer);
	this.menuGroup = hs.makeGroup({group:centerGroup, y:-50});
	this.caption = hs.makeText({text:"The Dollar Game", y:-150, fontSize:60, isBold:true, font:"big", color:"black", group:this.menuGroup});
	this.dollar = hs.makeSprite({frame:"dollar", group:this.menuGroup, y:-50});
	this.playSavedButton = hs.makeButton({sprite:[{text:"play big graph from video", fontSize:35, color:"black"}], group:this.menuGroup, y:50, func:this.pressPlayFromVideo, context:this});
	var bestCaption;
	if (graph.bestSolution)
		bestCaption = "best: "+graph.bestSolution.length;
	else
		bestCaption = "best: 0";
	this.bestText = hs.makeText({text:bestCaption, y:50+70, fontSize:30, isBold:true, color:"black", group:this.menuGroup});
	this.bestText.x -= this.bestText.width/2+20;
	this.replayButton = hs.makeButton({sprite:[{text:"replay", fontSize:30, color:"black"}], group:this.menuGroup, y:50+70, func:this.pressPlayReplay, context:this});
	this.replayButton.x += this.bestText.width/2+20;
	if (!graph.bestSolution)
		this.replayButton.alpha = .5;

	this.playRandomButton = hs.makeButton({sprite:[{text:"play random graph", fontSize:35, color:"black"}], group:this.menuGroup, y:150+70, func:this.pressPlayRandom, context:this});
	graph.isReplay = false;
}

ui.createRestartConfirm = function() {
	this.restartConfirmGroup = hs.makeGroup({group:centerGroup});
	this.restartConfirmCover = hs.makeSprite({frame:"dollar", group:this.restartConfirmGroup});
	this.restartConfirmCover.alpha = 0;
	this.restartConfirmCover.inputEnabled = true;
	this.restartConfirmCover.input.useHandCursor = false;
	this.restartConfirmCover.hitArea = new Phaser.Rectangle(-game.width/2, -game.height/2, game.width, game.height);
	this.restartConfirmCover.events.onInputDown.add(function() {
		this.closeRestartConfirm();
	}, this);

	this.restartConfirmFrame = hs.makeSprite({frame:"restart_confirm", group:this.restartConfirmGroup});
	this.restartConfirmFrame.inputEnabled = true;
	this.restartConfirmFrame.input.useHandCursor = false;

	hs.makeText({text:"restart?", y:-50, fontSize:35, isBold:true, color:"black", group:this.restartConfirmGroup});
	hs.makeButton({sprite:[{text:"yes", fontSize:35, color:"black"}], group:this.restartConfirmGroup, y:40, x:-80, func:function() {
		graph.restart();
		this.closeRestartConfirm();
	}, context:this});
	hs.makeButton({sprite:[{text:"no", fontSize:35, color:"black"}], group:this.restartConfirmGroup, y:40, x:80, func:function() {
		this.closeRestartConfirm();
	}, context:this});
	hs.makeButton({sprite:[{text:"x", fontSize:35, color:"black"}], group:this.restartConfirmGroup, y:-90, x:135, size:70, func:function() {
		this.closeRestartConfirm();
	}, context:this});
}

ui.closeRestartConfirm = function() {
	this.restartConfirmGroup.destroy(true);
}

ui.createNewRandomConfirm = function() {
	this.newRandomConfirmGroup = hs.makeGroup({group:centerGroup});
	this.newRandomConfirmCover = hs.makeSprite({frame:"dollar", group:this.newRandomConfirmGroup});
	this.newRandomConfirmCover.alpha = 0;
	this.newRandomConfirmCover.inputEnabled = true;
	this.newRandomConfirmCover.input.useHandCursor = false;
	this.newRandomConfirmCover.hitArea = new Phaser.Rectangle(-game.width/2, -game.height/2, game.width, game.height);
	this.newRandomConfirmCover.events.onInputDown.add(function() {
		this.closeNewRandomConfirm();
	}, this);

	this.newRandomConfirmFrame = hs.makeSprite({frame:"restart_confirm", group:this.newRandomConfirmGroup});
	this.newRandomConfirmFrame.inputEnabled = true;
	this.newRandomConfirmFrame.input.useHandCursor = false;

	hs.makeText({text:"new graph?", y:-50, fontSize:35, isBold:true, color:"black", group:this.newRandomConfirmGroup});
	hs.makeButton({sprite:[{text:"yes", fontSize:35, color:"black"}], group:this.newRandomConfirmGroup, y:40, x:-80, func:function() {
		this.pressNewRandom();
		this.closeNewRandomConfirm();
	}, context:this});
	hs.makeButton({sprite:[{text:"no", fontSize:35, color:"black"}], group:this.newRandomConfirmGroup, y:40, x:80, func:function() {
		this.closeNewRandomConfirm();
	}, context:this});
	hs.makeButton({sprite:[{text:"x", fontSize:35, color:"black"}], group:this.newRandomConfirmGroup, y:-90, x:135, size:70, func:function() {
		this.closeNewRandomConfirm();
	}, context:this});
}

ui.closeNewRandomConfirm = function() {
	this.newRandomConfirmGroup.destroy(true);
}

ui.pressPlayFromVideo = function() {
	this.menuGroup.destroy(true);
	graph.isFromVideo = true;
	this.create();
	graph.loadLocal();
	if (graph.isWin) {
		graph.load(graphFromVideo);
		graph.isWin = false;
		graph.stepNum = 0;
		graph.solution = [];
	}
	graph.show();
	this.updateSteps();
}

ui.pressPlayReplay = function() {
	if (!graph.bestSolution || !graph.bestSolution.length)
		return;
	graph.isReplay = true;
	this.menuGroup.destroy(true);
	graph.isFromVideo = true;
	this.create();
	this.changeModeButton.destroy(true);
	graph.load(graphFromVideo);
	graph.show();
	graph.stepNum = 0;
	graph.solution = [];
	this.updateSteps();
	graph.playReplay();
}

ui.pressPlayRandom = function() {
	this.menuGroup.destroy(true);
	graph.isFromVideo = false;
	this.create();
	graph.loadLocal();
	if (graph.isWin) {
		graph.createRandom();
		graph.isWin = false;
		graph.stepNum = 0;
		graph.solution = [];
	}
	graph.show();
	graph.isWin = false;
	this.updateSteps();
}

ui.pressNewRandom = function() {
	graph.createRandom();
	graph.isWin = false;
	graph.stepNum = 0;
	graph.solution = [];
	graph.restart();
	graph.show();
	this.updateSteps();
	this.clearWin();
}

ui.changeMode = function() {
	graph.isCollectMode = !graph.isCollectMode;
	ui.showMode();
}

ui.showMode = function() {
	if (!this.collectModeText)
		return;
	if (graph.isCollectMode) {
		this.collectModeText.visible = true;
		this.spreadModeText.visible = false;
	} else {
		this.collectModeText.visible = false;
		this.spreadModeText.visible = true;
	}
}