"use strict";

var preloaderState = {};

preloaderState.preload = function() {
	game.stage.backgroundColor = "#d29a71";
	this.group = hs.makeGroup({group:centerGroup});
	hs.makeText({text:"Loading...", color:"black", fontSize:30, group:this.group});
	hs.loadFromLS("dev image", "circle.png  craft_paper.jpg  dollar.png  line.png restart_confirm.png button.png");
	this.resize();
}

preloaderState.resize = function() {
	this.group.x = game.width * .5;
	this.group.y = game.height * .5;
}

preloaderState.create = function() {
	game.state.start("gameState");
}