"use strict";

var bootState = {};

bootState.preload = function() {  
	game.scale.pageAlignHorizontally = true;
	game.scale.pageAlignVertically = true;
	game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
	game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
	game.scale.setResizeCallback(hs.resizeCallback, hs);
	game.time.advancedTiming = true;
}

bootState.create = function() {
	game.state.start('preloaderState');
}